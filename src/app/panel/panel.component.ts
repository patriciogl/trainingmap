import { Component, OnInit, Input, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
declare let L;
@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  @Input() map;
  @Input() comunas;
  @Input() liceos;
  @Input() comunaShown;
  @Output() menuState = new EventEmitter();
  @Output() showNombreComuna = new EventEmitter();
  @Output() showLiceos = new EventEmitter();
  isLiceoShown;
  isNombreShown;
  isUserClickShown;
  lastZoom;
  constructor() {
    this.isNombreShown = false;
    this.isLiceoShown = true;
    this.isUserClickShown = false;

  }
  toggleMenu() {
    // 1-line if statement that toggles the value:
    this.menuState.emit()
  }

  ngOnInit() {



  }
  ngAfterViewInit() {
    


    //this.map.on('click', this.NOMBREFUNCION.bind(this));
  }

  onClickShowNombreComuna() {
    this.showNombreComuna.emit();
  }

  onClickShowLiceos() {
    this.showLiceos.emit();
  }
}
