import { Component, OnInit, ContentChild, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import polylabel from 'src/assets/polylines';
import 'node_modules/leaflet-sidebar-v2/js/leaflet-sidebar.js';
import { ApiVisorTpService } from './service/api-visor-tp.service';


declare let L;
declare let omnivore;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('rightPanel', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('rightButton', [
      state('in', style({
        transform: 'translate3d(-600%, ' + (innerHeight / 2 - 25) + 'px, 0)'
      })),
      state('out', style({
        transform: 'translate3d(0, ' + (innerHeight / 2 - 25) + 'px, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('topPanel', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(0, -100%, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('topButton', [
      state('in', style({
        transform: 'translate3d(-' + (innerWidth / 2 - 25) + 'px, 400%, 0)'
      })),
      state('out', style({
        transform: 'translate3d(-' + (innerWidth / 2 - 25) + 'px, 7vh, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'training-maps';
  menuState: string = 'out';
  topMenuState: string = 'out';
  lastVisited: String = '';
  map;
  comunas;
  liceos;
  isLiceoShown = true;
  isNombreComunaShown = false;
  isUserClickShown = false;
  lastLayer;
  macroZones = []
  macro1 = ["#ff69b4", "SAAVEDRA", "TOLTEN", "CARAHUE", "NUEVA IMPERIAL", "TEODORO SCHMIDT"];
  macro2 = ["#006400", "PUREN", "LOS SAUCES", "TRAIGUEN", "GALVARINO", "CHOLCHOL", "LUMACO"];
  macro3 = ["#E0FFFF", "TEMUCO", "PADRE LAS CASAS"];
  macro4 = ["#fffdd0", "FREIRE", "PITRUFQUEN", "GORBEA", "LONCOCHE"];
  macro5 = ["#FEC8D8", "VICTORIA", "LAUTARO", "PERQUENCO"];
  macro6 = ["#6B8E23", "ANGOL", "RENAICO", "COLLIPULLI", "ERCILLA"];
  macro7 = ["#90EE90", "CURACAUTIN", "LONQUIMAY", "MELIPEUCO", "VILCUN", "CUNCO"];
  macro8 = ["#FD8D3C", "PUCON", "VILLARRICA", "CURARREHUE"];

  constructor(private apiService: ApiVisorTpService) {
    this.apiService.getAllLiceo().subscribe(res =>{
      this.liceos = res;
      this.addLiceos();
    })
  }

  ngOnInit(): void {
    //let watercolor = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', { ext: 'jpg' });
    let openStreetMap = L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { ext: 'png' });
    //let noLabels = L.tileLayer("https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png", { ext: 'png' });
    let macroZones = [this.macro1, this.macro2, this.macro3, this.macro4, this.macro5, this.macro6, this.macro7, this.macro8];
    //this.map = L.map('map').setView([-38.73577442650896, -72.59057521820068], 8);
    this.map = L.map('map', {
      center: [-38.73577442650896, -72.59057521820068],
      zoom: 8,
      maxZoom: 18,
      zoomControl: false,
      tap: false,
      layers: [openStreetMap]
    })
    let baseMaps = {
      //  'waterColor': watercolor,
      //  'noLabels': noLabels,
      'openStreetMap': openStreetMap
    }

    //L.control.layers(baseMaps, null, { position: 'bottomleft' }).addTo(this.map);

    //cantidad de tiles extra a renderizar
    this.map.getRenderer(this.map).options.padding = 1;

    this.comunas = omnivore.kml('./src/assets/sectores/doc.kml')
      .on('ready', e => {
        this.map.fitBounds(this.comunas.getBounds());
        this.comunas.eachLayer(layer => {
          colorMacrozone(macroZones, layer);
          /*   let center = polylabel(layer.feature.geometry.coordinates, 1)
             L.marker([center[1], center[0]]).bindTooltip(layer.feature.properties.name, { permanent: false, className: "my-labels", offset: [0, 0], opacity: 0.9 }).addTo(this.map)
           */
          layer.bindTooltip(layer.feature.properties.name, { permanent: false, className: "my-labels", offset: [0, 0], opacity: 0.9 })
            .on({
              'click': a => { this.onComunaSelect(layer) },
              'mouseover': a => { this.highlight(layer) },
              'mouseout': a => { colorMacrozone(macroZones, layer) }
            });
        })
      })
    //.addTo(this.map)
    this.map.on('zoomend', e => {
      this.mapOnZoom();
    });
   //this.addLiceos().addTo(this.map)
  }

  ngAfterViewInit() {
    this.comunas.addTo(this.map);
    //this.map.removeLayer(this.comunas)
  }

  resetHighlight(layer) {
    this.comunas.resetStyle(layer);
  }
  highlight(layer) {
    layer.setStyle({
      weight: 5,
      color: '#666',
      dashArray: '',
      fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
    }
  }
  addLiceos() {
    
    this.liceos.forEach(element => {
      console.log(element)
      let marker = L.marker([element.latitud, element.longitud]).bindPopup(
        "nombre:"+ element.nom_rbd
      );
      L.layerGroup([marker]).addTo(this.map);
    });

    let littleton = L.marker([-38.734268005053316, -72.59284973144531
    ]).bindPopup('This is Littleton, CO.'),
      denver = L.marker([-38.76338652228421, -72.59156227111816
      ]).bindPopup('This is Denver, CO.')
    
  }

  mapOnZoom() {
    if ((this.map.getZoom()) > 9 && !this.isUserClickShown) {
      !this.isNombreComunaShown && (this.isNombreComunaShown = !this.isNombreComunaShown);
      setTimeout(() =>
        this.comunas.eachLayer(tt => {
          let tooltip = tt.getTooltip();
          tt.unbindTooltip();
          tt.bindTooltip(tooltip, { permanent: true })
        })
        , 10);
    } else if (this.isNombreComunaShown && !this.isUserClickShown) {
      this.isNombreComunaShown = !this.isNombreComunaShown;
      this.comunas.eachLayer(tt => {
        let tooltip = tt.getTooltip();
        tt.unbindTooltip();
        tt.bindTooltip(tooltip, { permanent: false })
      });
    }
  }

  onComunaSelect(layer) {
    if (this.lastVisited !== layer.feature.properties.name) {
      this.lastVisited = layer.feature.properties.name;
      this.map.fitBounds(layer.getBounds());
      /* this.comunas.eachLayer(capa => {
        let opacity = 0.2;
        if (capa.feature.properties.name == layer.feature.properties.name) opacity = 0;
        capa.setStyle({
          fillOpacity: opacity
        })
      }) */
    }


    //if (reset) this.map.fitBounds(this.comunas.getBounds());
  }



  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }
  toggleTopMenu() {
    this.topMenuState = this.topMenuState === 'out' ? 'in' : 'out';
  }
  onClickShowLiceos() {
    if (this.isLiceoShown) {
      this.isLiceoShown = !this.isLiceoShown;
      this.liceos.remove();
    } else {
      this.isLiceoShown = !this.isLiceoShown;
      this.liceos.addTo(this.map);
    }
  }
  onClickShowNombreComuna() {
    this.isUserClickShown = true;
    if (this.isNombreComunaShown) {
      this.isNombreComunaShown = !this.isNombreComunaShown;
      this.comunas.eachLayer(f => {
        let tooltip = f.getTooltip();
        f.unbindTooltip();
        f.bindTooltip(tooltip, { permanent: false })
      });
    } else {
      this.isNombreComunaShown = !this.isNombreComunaShown;
      this.comunas.eachLayer(f => {
        let tooltip = f.getTooltip();
        f.unbindTooltip();
        f.bindTooltip(tooltip, { permanent: true })
      });
    }
  }


}

function colorMacrozone(macroZones, layer) {
  let paintingColor = "";
  macroZones.forEach(element => {
    if (element.indexOf(layer.feature.properties.name) !== -1) {
      paintingColor = element[0];
      return;
    }
  });
  layer.setStyle({
    color: "white",
    dashArray: 3,
    weight: 2,
    fillColor: paintingColor,
    fillOpacity: 0.5,
    smoothFactor: 2,
  })
}
