export interface Liceo {
    cod_com: number;
    cod_dependencia: number;
    cod_nivel: number;
    dgv_rbd: number;
    direccion: string;
    email1: string;
    email2: string;
    id_estado: number;
    ive: number;
    latitud: number;
    longitud: number;
    matri_hom: number;
    matri_muj: number;
    matricula: number;
    modalidad: string;
    nom_director: string;
    nom_rbd: string;
    num_docentes: number;
    pace: number;
    rbd_liceo: number;
    run_sostenedor: number;
    telefono1: number;
    telefono2: number;
    web: string;
}