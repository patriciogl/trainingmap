import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Liceo } from '../model/liceo';
const apiUrl = '//visortp.iie.cl:8000/api';
const liceo = '/tpliceos';

@Injectable({
    providedIn: 'root'
})
export class ApiVisorTpService {
    constructor(private _http: HttpClient){

    }

    getAllLiceo(): Observable<Liceo[]>{
        return this._http.get<Liceo[]>(apiUrl+liceo);
    }
}